<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */


/**
 * Implements hook_entity_info_alter().
 */
function omega_dlr_entity_info_alter(&$entity_info) {
  //let's override the callback for taxonomy term paths, only for vocab "my-vocabulary":
  $entity_info['taxonomy_term']['bundles']['news_tags']['uri callback'] = 'omega_dlr_news_term_uri';
}

/**
 * Helper function to implement term_uri($term).
 */
function omega_dlr_news_term_uri($term) {
  //in this case we have a custom view that takes a taxonomy term as contextual filter:
  return array('path' => 'news/tag/' . $term->name);
}